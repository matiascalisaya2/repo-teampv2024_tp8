package ar.edu.unju.fi.TP7_teamPV;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tp7TeamPvPruebaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tp7TeamPvPruebaApplication.class, args);
		
		/*
		 * GRUPO: repo-teamPV2024
		 * INTEGRANTES:
		 * 				Judith Aylen Ruth Colqui
		 * 				Noelia Raquel Ruth Acho
		 * 				Anabella Genesis Acho
		 * 				Matias Waldo Calisaya
		 * 
		 */
	
	}

}
