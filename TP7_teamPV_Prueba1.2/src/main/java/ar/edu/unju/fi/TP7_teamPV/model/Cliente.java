package ar.edu.unju.fi.TP7_teamPV.model;

import java.time.LocalDate; 
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "CLIENTES")
@Component

public class Cliente {
	//AATRIBUTOS
	@Column(name = "TIPODOCUMENTO")
		private String tipoDocumento;
	
	@Id
	@Column(name = "NRODOCUMENTO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int nroDocumento;
	
	@Column(name = "APELLIDO")
		private String Apellido;
	
	@Column(name = "NOMBRE")
		private String Nombre;
	
	@Column(name = "EMAIL")
		private String email;
	
	@Column(name = "PASSWORD")
		private String password;
	
	@Column(name = "FECHA_NACIMIENTO")
		@DateTimeFormat (pattern = "yyyy-MM-dd")
		private LocalDate fechaNacimiento;
		
	@Column(name = "EDAD")
		private int edad;
	
	@Column(name = "CODAREATELEFENO")
		private int codigoAreaTelefono;
	
	@Column(name = "NROTELEFONO")
		private int nroTeléfono;
	
	@Column(name = "FECHA_ULTIMACOMPRA")
		@DateTimeFormat (pattern = "yyyy-MM-dd")
		private LocalDate fechaUltimaCompra;
	
	@Column(name = "CALCULAR1")
		private String calular1;
	
	@Column(name = "CALCULAR2")
		private String calular2;
	
	
	@Column(name = "CALCULAR3")
		private String calular3;
	
	//CONSTRUCTORES
		
	
	public Cliente() {
		super();
	}

	public Cliente(String tipoDocumento, String apellido, String nombre, String email, String password,
			LocalDate fechaNacimiento, int codigoAreaTelefono, int nroTeléfono, LocalDate fechaUltimaCompra) {
		super();
		this.tipoDocumento = tipoDocumento;
		Apellido = apellido;
		Nombre = nombre;
		this.email = email;
		this.password = password;
		this.fechaNacimiento = fechaNacimiento;
		this.codigoAreaTelefono = codigoAreaTelefono;
		this.nroTeléfono = nroTeléfono;
		this.fechaUltimaCompra = fechaUltimaCompra;
	}

	//GETERS Y SETERS
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public int getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(int nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public String getApellido() {
		return Apellido;
	}
	public void setApellido(String apellido) {
		Apellido = apellido;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public int getCodigoAreaTelefono() {
		return codigoAreaTelefono;
	}
	public void setCodigoAreaTelefono(int codigoAreaTelefono) {
		this.codigoAreaTelefono = codigoAreaTelefono;
	}
	public int getNroTeléfono() {
		return nroTeléfono;
	}
	public void setNroTeléfono(int nroTeléfono) {
		this.nroTeléfono = nroTeléfono;
	}
	public LocalDate getFechaUltimaCompra() {
		return fechaUltimaCompra;
	}
	public void setFechaUltimaCompra(LocalDate fechaUltimaCompra) {
		this.fechaUltimaCompra = fechaUltimaCompra;
	}
	public String getCalular1() {
		return calular1;
	}
	public void setCalular1(String calular1) {
		this.calular1 = calular1;
	}
	public String getCalular2() {
		return calular2;
	}
	public void setCalular2(String calular2) {
		this.calular2 = calular2;
	}
	public String getCalular3() {
		return calular3;
	}
	public void setCalular3(String calular3) {
		this.calular3 = calular3;
	}
	
		
		
}
