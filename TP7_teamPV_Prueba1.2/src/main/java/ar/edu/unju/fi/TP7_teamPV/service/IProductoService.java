package ar.edu.unju.fi.TP7_teamPV.service;

import java.util.List;

import ar.edu.unju.fi.TP7_teamPV.model.Producto;



public interface IProductoService {

	public void agregarProducto(Producto producto);
	
	public List<Producto> listaProductos();
	
	public Producto ultimoproducto();
	
	public Producto buscarEmpleado(int codigo);
	
	public void eliminarEmpleado(Producto prod);
	//METODOS AGREGADOS PARA TP7
	
		public String mostrarNombreProducto(int codigo);
		public String mostrarPrecioProducto(int codigo);
		public String mostrarMarcaProducto(int codigo);
}
