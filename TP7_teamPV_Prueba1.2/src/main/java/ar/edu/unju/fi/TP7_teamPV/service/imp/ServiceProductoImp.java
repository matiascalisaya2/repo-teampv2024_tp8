package ar.edu.unju.fi.TP7_teamPV.service.imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import ar.edu.unju.fi.TP7_teamPV.model.Producto;
import ar.edu.unju.fi.TP7_teamPV.service.IProductoService;

@Service
public class ServiceProductoImp implements IProductoService {
	List<Producto> listaProtuctos = new ArrayList<>();

	@Override
	public void agregarProducto(Producto producto) {
		Producto produ = buscarEmpleado(producto.getCodigo());
		if (produ == null) {
			listaProtuctos.add(producto);
		} else {
			int pos = listaProtuctos.indexOf(produ);
			listaProtuctos.set(pos, producto);
		}

	}

	@Override
	public List<Producto> listaProductos() {

		return listaProtuctos;
	}

	@Override
	public Producto ultimoproducto() {
		Producto ultimoProducto = listaProtuctos.get(listaProtuctos.size() - 1);
		return ultimoProducto;
	}

	@Override
	public Producto buscarEmpleado(int codigo) {
		Producto productoBuscado = null;

		for (Producto produc : listaProtuctos) {
			if (produc.getCodigo() == codigo) {
				productoBuscado = produc;
				break;
			}
		}
		return productoBuscado;
	}

	@Override
	public void eliminarEmpleado(Producto prod) {
		listaProtuctos.remove(prod);

	}

	// METODOS AÑADIDOS PARA EL TP7

	@Override
	public String mostrarNombreProducto(int codigo) {
		Producto productoBuscado = null;

		for (Producto produc : listaProtuctos) {
			if (produc.getCodigo() == codigo) {
				productoBuscado = produc;
				break;
			}
		}
		return productoBuscado.getNombre();
	}

	@Override
	public String mostrarPrecioProducto(int codigo) {
		Producto productoBuscado = null;

		for (Producto produc : listaProtuctos) {
			if (produc.getCodigo() == codigo) {
				productoBuscado = produc;
				break;
			}
		}
		return " "+productoBuscado.getPrecio();
	}

	@Override
	public String mostrarMarcaProducto(int codigo) {
		Producto productoBuscado = null;

		for (Producto produc : listaProtuctos) {
			if (produc.getCodigo() == codigo) {
				productoBuscado = produc;
				break;
			}
		}
		return productoBuscado.getMarca();
	}

}
