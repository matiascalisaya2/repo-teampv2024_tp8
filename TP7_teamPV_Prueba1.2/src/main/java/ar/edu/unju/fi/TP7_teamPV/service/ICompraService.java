package ar.edu.unju.fi.TP7_teamPV.service;

import java.util.List;

import ar.edu.unju.fi.TP7_teamPV.model.Compra;

public interface ICompraService {
	public List<Compra> listarCompras();
	public void agregarCompra(Compra compra);
	public Compra buscarCompra(int id) ;
	public void eliminarCompra(Compra compra);

}
